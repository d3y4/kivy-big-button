import subprocess
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout

class DemoApp(App):

    def build(self):
        layout = BoxLayout(orientation='vertical')
        blue = (0, 0, 1.5, 2.5)
        red = (2.5, 0, 0, 1.5)
        btn =  Button(text='Go', background_color=blue, font_size=120)        
        btn.bind(on_press=self.callback)
        layout.add_widget(btn)
        return layout


    def callback(self, event):
        print("button pressed")  # test


DemoApp().run()
